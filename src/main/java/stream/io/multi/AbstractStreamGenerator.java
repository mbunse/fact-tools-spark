/*
 *  fact-tools-spark - FACT-tools for the streams-spark extension
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io.multi;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stream.io.SourceURL;
import stream.io.Stream;

import java.util.Map;
import java.util.Map.Entry;

/**
 * The AbstractStreamGenerator is a {@link stream.io.multi.MultiStream}
 * generating its inner streams. Primarily, this is used to elegantly create
 * sets of streams for distributed processing.
 * 
 * Note: In the future, some abstract generator may be designed that is more
 * generic in terms of being viable to generate other logic of inner stream
 * union (like, e.g., merging streams).
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 */
public abstract class AbstractStreamGenerator extends LazySeqMultiStream
		implements StreamGenerator {

    private static Logger log = LoggerFactory.getLogger(AbstractStreamGenerator.class);

    protected long streamLimits = Long.MAX_VALUE;
    private boolean areStreamsPrepared = false;

    /**
     * Default constructor only to be used by the streams framework when parsing XML.
     */
    public AbstractStreamGenerator() {
        super();
    }
    
	/**
	 * The recommended constructor setting the directory to generate streams
	 * from.
	 * 
	 * @param url
	 *            The directory to generate streams from
	 */
    public AbstractStreamGenerator(SourceURL url) {
        this();
        this.setUrl(url);
    }

    @Override
    public void init() throws Exception {

        if (!this.areStreamsPrepared)
            this.prepareStreams();

        super.init();    // block error happens here

    }

    @Override
    public Map<String, Stream> getStreams() {

        if (!this.areStreamsPrepared)
            this.prepareStreams();

        return super.getStreams();

    }

	/**
	 * Generates the inner streams using {@link StreamGenerator#generateStreams()}.
	 * Checks if they are reasonable and adds them to this MultiStream.
	 * 
	 * @throws IllegalStateException
	 *             Thrown, when this method is called more than once
	 */
    private void prepareStreams() throws IllegalStateException {

        if (this.areStreamsPrepared)
            throw new IllegalStateException(this.getClass().getCanonicalName()
                    + " with id " + this.getId()
                    + " tried to generate it's streams more than once.");

        // generate stream set
        Map<String, Stream> gen = this.generateStreams();
		if (gen != null && gen.size() > 0) {
			
			log.info("{} generated {} streams.", this.getId(), gen.size());

	        // add generated streams to set of substreams
			for (Entry<String, Stream> e : gen.entrySet()) {
				
				// only add if not already present
				if (this.streams == null
						|| !this.streams.containsKey(e.getKey())) {

                    this.addStream(e.getKey(), e.getValue());
                    if (e.getValue() != null)
                        e.getValue().setLimit(this.streamLimits);

                } else {

                    log.warn("{} generated a stream for source {} that is omitted "
                                    + "because it is associated to id {}, which is already used.",
                            this.getClass().getCanonicalName(), this.getId(), e.getKey());

                }
            }

        } else {

            log.warn("{} did not generate any stream for source {}.",
                    this.getClass().getCanonicalName(), this.getId());

        }

        this.areStreamsPrepared = true;

    }

	/**
	 * @return The limit of generated inner streams.
	 * 
	 * @see stream.io.Stream#getLimit()
	 */
    public long getStreamLimits() {
        return streamLimits;
    }

	/**
	 * @param streamLimits
	 *            The limit parameter for generated inner streams.
	 *            
	 * @see stream.io.Stream#setLimit(Long)
	 */
    public void setStreamLimits(long streamLimits) {
        if (streamLimits > 0)
            this.streamLimits = streamLimits;
        else
            throw new IllegalArgumentException("Parameter streamsLimit has to be > 0!");
    }

}
