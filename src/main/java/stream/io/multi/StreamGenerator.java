/*
 *  fact-tools-spark - FACT-tools for the streams-spark extension
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io.multi;


import java.util.Map;

import stream.io.Stream;

/**
 * Generates the inner streams of a {@link stream.io.multi.MultiStream}.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 *
 */
public interface StreamGenerator {

	/**
	 * Generates the inner streams of a {@link stream.io.multi.MultiStream}.
	 * 
	 * @return A mapping from IDs to inner streams of the
	 *         {@link stream.io.multi.MultiStream}.
	 * @see MultiStream#getStreams()
	 */
	Map<String, Stream> generateStreams();

}
