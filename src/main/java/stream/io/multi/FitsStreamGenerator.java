/*
 *  fact-tools-spark - FACT-tools for the streams-spark extension
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io.multi;


import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fact.io.FITSStream;
import stream.Data;
import stream.io.SourceURL;
import stream.io.Stream;


/**
 * Generates a {@link MultiStream} consisting of {@link FITSStream}s. For every
 * .fits.gz file matching a regular expression (in a given root directory), one
 * inner stream is generated.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * @author Mohamed Asmi &lt;mohamed.asmi@tu-dortmund.de&gt;
 * @author Karl Stelzner &lt;karl.stelzner@tu-dortmund.de&gt;
 * 
 */
@SuppressWarnings("deprecation")
public class FitsStreamGenerator extends AbstractStreamGenerator {

    private static Logger log = LoggerFactory.getLogger(FitsStreamGenerator.class);

    private String regex = ".*[^(\\.drs)]\\.fits\\.gz";    // filters out DRS files
    private int maxNumStreams = Integer.MAX_VALUE;    // default: no limit

	/**
	 * @param url
	 *            The directory to generate streams from
	 */
    public FitsStreamGenerator(SourceURL url) {
        super(url);
    }

    @Override
    public Map<String, Stream> generateStreams() {
    	
    	log.info("{} generating up to {} fits streams...", this.getId(), this.maxNumStreams);
        Map<String, Stream> genStreams = new HashMap<>();

        try {

            // extract the URI of the file system (yes, this is weird)
            Pattern fsURIpattern = Pattern.compile("^[a-z]+://[^:]+:[0-9]+"); // matches "protocol://host:port"
            Matcher fsMatcher = fsURIpattern.matcher(this.getUrl().toString());
            if (!fsMatcher.find())
                throw new Exception("Invalid URL format, filesystem could not be determined: "
                		+ this.getUrl().toString());

            // initialize file system with the extracted URI
            URI filesystemURI = new URI(fsMatcher.group());
            FileSystem hdfs = FileSystem.get(filesystemURI, new Configuration());

            // list files "remotely"
            RemoteIterator<LocatedFileStatus> statusIter =
                    hdfs.listFiles(new Path(this.getUrl().toString()), true);
            
            if (statusIter.hasNext()) {

                // iterate over files
                while (statusIter.hasNext() && genStreams.size() < this.maxNumStreams) {
                    String path = statusIter.next().getPath().toString();
                    if (path.matches(this.regex)) {
                        genStreams.put(path, null);
                    }
                }

            } else {
            	log.warn("Could not find any files in root directory {}!",
            			this.getUrl().toString());
            }
        

        } catch (Exception e) {
            e.printStackTrace();	// this is bad (genStreams may be empty, causing system to stop)
        }

        if (genStreams.size() < 1) {

            log.warn("No file in {} matches the regular expression '{}'!",
            		this.getUrl().toString(), this.regex);

        }

        return genStreams;

    }

    @Override
    public void addStream(String id, Stream stream) {
    	
    	// lazy initialization of streams map (required because not transferred)
    	if (this.streams == null) {
    		
    		// sub-streams require lazy generation and limits set appropriately
            this.streams = new HashMap<String, Stream>() {
    			private static final long serialVersionUID = -62685817933549711L;

    			/* Lazy generation of streams when #get(Object) is called */
    			@Override
                public Stream get(Object key) {
                    Stream s = super.get(key);
                    if (s == null) {
                        try {
                        	
                        	// anonymous fits stream without MonitorStateException
                            s = new FITSStream(new SourceURL((String) key)) {

								@Override
								public Data readNext() throws Exception {
									try {
										return super.readNext();
									} catch (IllegalMonitorStateException e) {
										log.warn("Closing FITSStream that underlies this MultiStream threw an IllegalMonitorStateException. I stop reading from it.");
										return null;
									}
									
								}
								
                            };
                            s.setLimit(FitsStreamGenerator.this.streamLimits);
                            log.trace("New Stream was generated from: {}", key.toString());
                            super.put((String) key, s);
                            
                        } catch (Exception e) {
                            log.error(e.getMessage());
                        }
                    }
                    return s;
                }
            };
            
            this.additionOrder = new ArrayList<String>();
    		
    	}

        streams.put(id, stream);
        additionOrder.add(id);
        log.trace("Added Stream {}", id);
        
    }

    /**
     * @return Regular expression for FITS file names
     */
    public String getRegex() {
        return regex;
    }

	/**
	 * @param regex
	 *            Regular expression for FITS file names
	 */
    public void setRegex(String regex) {
        this.regex = regex;
    }

    /**
     * @return The maximum number of streams to be generated
     */
    public int getMaxNumStreams() {
        return maxNumStreams;
    }

	/**
	 * @param maxNumStreams
	 *            The maximum number of streams to be generated
	 */
    public void setMaxNumStreams(int maxNumStreams) {
        this.maxNumStreams = maxNumStreams;
    }

    /**
     * @return The URL of a root directory where files that may match the
     * regular expression may be located.
     */
    @Override
    public SourceURL getUrl() {
        return super.getUrl(); // Only overridden to fix javaDoc!
    }

	/**
	 * @param url
	 *            The URL of a root directory where files that may match the
	 *            regular expression may be located.
	 */
    @Override
    public void setUrl(SourceURL url) {
        super.setUrl(url);    // Only overridden to fix javaDoc!
    }

}
