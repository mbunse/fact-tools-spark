/*
 *  fact-tools-spark - FACT-tools for the streams-spark extension
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io;

import java.util.HashSet;
import java.util.Set;

import stream.Data;
import stream.Keys;
import stream.Processor;
import stream.runtime.DProcess;

/**
 * Only keep a specified set of {@link Data} item keys. The specified
 * workerIdKey is always kept (default is
 * {@link DProcess#DEFAULT_WORKER_ID_KEY}).
 *
 * @author Karl Stelzner &lt;karl.stelzner@tu-dortmund.de&gt;
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 */
public class FilterKeys implements Processor {

	private Keys keys;
	private String workerIdKey = DProcess.DEFAULT_WORKER_ID_KEY;

	public FilterKeys() {
		// Nothing to do
	}

	@Override
	public Data process(Data data) {

		if (keys == null)
			return data;

		// set up the set of keys to remove
		Set<String> keepSet = keys.select(data);
		Set<String> removeSet = new HashSet<String>();
		for (String key : data.keySet())
			if (!keepSet.contains(key))
				removeSet.add(key);
		
		// apply key removal (never remove worker ID)
		for (String key : removeSet) {
			if (!key.equals(this.workerIdKey))
				data.remove(key);
		}
		
		return data;
		
	}
	
	/**
	 * @return The keys to keep. All others are filtered out.
	 */
	public Keys getKeys() {
		return keys;
	}

	/**
	 * @param keys
	 *            The keys to keep. All others are filtered out.
	 */
	public void setKeys(Keys keys) {
		this.keys = keys;
	}

	/**
	 * @return The key for the worker ID, which is always kept.
	 */
	public String getWorkerIdKey() {
		return workerIdKey;
	}

	/**
	 *
	 * @param workerIdKey
	 *            The key for the worker ID, which is always kept.
	 */
	public void setWorkerIdKey(String workerIdKey) {
		this.workerIdKey = workerIdKey;
	}
	

}
