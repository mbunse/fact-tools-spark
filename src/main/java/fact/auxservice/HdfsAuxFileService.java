/*
 *  fact-tools-spark - FACT-tools for the streams-spark extension
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package fact.auxservice;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.io.SourceURL;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashMap;

/**
 * The default {@link AuxFileService}, extended to support auxiliary files
 * stored on HDFS.
 *
 * @author Michael May &lt;michael.may@tu-dortmund.de&gt;
 * 
 */
public class HdfsAuxFileService extends AuxFileService {
	
	Logger log = LoggerFactory.getLogger(HdfsAuxFileService.class);

    @Override
    public HashMap<AuxiliaryServiceName, SourceURL> findAuxFileUrls(SourceURL auxFolder)
    		throws FileNotFoundException {
    	
        final HashMap<AuxiliaryServiceName, SourceURL> m = new HashMap<>();

        try {
            FileSystem hdfs = FileSystem.get(URI.create(auxFolder.toString()), new Configuration());

            RemoteIterator<LocatedFileStatus> statusIter =
                    hdfs.listFiles(new Path(auxFolder.toString()), true); // recursive listing all files

            if (statusIter.hasNext()) {
            	
            	while (statusIter.hasNext()) {
                    Path file = statusIter.next().getPath();
                    String fName = file.getName();
                    if (fName.endsWith(".fits")) {
                    	
                        try {
                        	
                            // get name of auxiliary file by removing the date string (first 9 characters) and the file ending
                            fName = fName.substring(9, fName.length() - 5);
                            AuxiliaryServiceName sName = AuxiliaryServiceName.valueOf(fName);
                            m.put(sName, new SourceURL(file.toString()));
                            log.info("Created service {}", sName);
                            
                        } catch (IllegalArgumentException e) {
                            log.debug("{} is no service name. Skipping {}.", fName, file);
                        } catch (MalformedURLException e) {
                            log.error("Could not create path to file {}", file);
                        } catch (StringIndexOutOfBoundsException e) {
                        	log.error("Could not obtain service name from file {}", file);
                        } catch (Exception e) {
                        	log.error("Could not create service from file {}", file);
                        }
                    }
                }

            } else {
                log.warn("Could not find any file in {}", auxFolder.toString());
            }
        } catch (Exception e) {
            log.error("Failed to obtain files from HDFS", e);
        }

        return m;

    }
    
}

