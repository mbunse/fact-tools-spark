FACT-tools for the streams-spark extension
========


This project allows to execute the [FACT-tools](https://github.com/fact-project/fact-tools)
on an [Apache Spark](http://spark.apache.org/) cluster.
The project is based on [the streams-spark extension](https://bitbucket.org/mbunse/streams-spark) for
[the streams framework](https://sfb876.de/streams/).

The first version of this setup was developed by the Project Group 594 at the University of Technology in Dortmund.
It was supported by the Deutsche Forschungsgemeinschaft (DFG) within the Collaborative Research Center SFB 876
"Providing Information by Resource-Constrained Analysis", project C3.
Feel free to contribute!



## Usage
The project can be built with Maven:

      $ mvn package

Besides compiling the Java-Code, this also obtains the `submit-streams.sh` script from the streams-spark-submit module as published at the remote Maven repository.
You can use the script to submit streams jobs to a YARN cluster, i.e., to
run your analyses on Spark. Have a look the man page of the script:

      $ ./streams-submit.sh --help

The script is configured by the files located in the `conf/` directory,
which you can change when desired.
Changes to the script itself are discouraged because they will be undone during any Maven `clean` phase. Instead, try to improve the script at its development git repository [streams-spark](https://bitbucket.org/mbunse/streams-spark) and ask the developers to deploy your changes.

You may want to execute the Feature Extraction examples `analysis.xml` and
`analysis_mc.xml` as a starting point.
They are located in the `example` folder.



## Contact
Feel free to contact any of the current maintainers:
[Mirko Bunse](https://bitbucket.org/mbunse/),
[Michael May](https://bitbucket.org/maymic/),
and [Karl Stelzner](https://bitbucket.org/KarlSt/).
You may also want to have a look at [the issues page of the streams-spark repository](https://bitbucket.org/mbunse/streams-spark/issues).

Contributors to the first version are the participants of the PG 594, namely Mohamed Asmi, Alexander Bainczyk, Mirko Bunse,
Dennis Gaidel, Michael May, Christian Pfeiffer, Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
Carolin Wiethoff, and Lili Xu.